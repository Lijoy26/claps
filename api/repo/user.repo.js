const User = require("../models").users;

exports.findUser = async (id) => {
  const result = await User.findOne({
    where: {
      id,
    },
    attributes: ["name","email"],

    raw: true,
  });
  return result;
};

exports.findAllUsers = async () => {
  const result = await User.findAll({
    attributes: ["name","email"],
    raw: true,
  });
  return result;
};

exports.postUser = async (userData) => {
  const result = await User.create(userData);
  return result
}