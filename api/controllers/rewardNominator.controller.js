const express = require("express");

const router = express.Router();
const rewardNominator = require("../repo/rewardNominator.repo");
const errorCode = require("../common/error");
const errorhandler = require("../common/errorhandler");
const successhandler = require("../common/successhandler");

router.post("/", async (req, res, next) => {
    try {
  
      const result = await rewardNominator.postRewardNominator(req.body);
  
      const resobj = {
        body: {
          Nominators: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
      console.log(error)
      next(error);
    }
    return true;
  });

module.exports = router;