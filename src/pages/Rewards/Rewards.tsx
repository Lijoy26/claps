/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import {
  EmailIcon,
  FacebookIcon,
  FacebookMessengerIcon,
  HatenaIcon,
  InstapaperIcon,
  LineIcon,
  LinkedinIcon,
  LivejournalIcon,
  MailruIcon,
  OKIcon,
  PinterestIcon,
  PocketIcon,
  RedditIcon,
  TelegramIcon,
  TumblrIcon,
  TwitterIcon,
  ViberIcon,
  VKIcon,
  WeiboIcon,
  WhatsappIcon,
  WorkplaceIcon,
} from "react-share";
import { RewardsWrapper } from "./styled";

const RewardsPage = () => {
  const share = () => {
    alert("hi");
  };
  return (
    <RewardsWrapper>
      <header className="aui-main-header aui-pri-header">
        <a href="www.heart.org" className="aui-skip-content">
          Skip to main content
        </a>
        <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto">
          <a href="/" className="claps-logo" aria-label="Claps Logo" />
          <button
            className="navbar-toggler ml-2 px-0"
            type="button"
            data-toggle="collapse"
            data-target="#toggleNav"
            aria-controls="toggleNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="aha-icon-hamburger" />
          </button>
          <div
            className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
            id="toggleNav"
          >
            <ul className="navbar-nav mx-lg-3 flex-lg-row flex-column">
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Home</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Upcoming Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/rewards">Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/dashboard">Login</a>
                </button>
              </li>
              <button
                className="navbar-toggler ml-2 px-0"
                type="button"
                data-toggle="collapse"
                data-target="#toggleNav1"
                aria-controls="toggleNav1"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="aha-icon-hamburger" />
              </button>
              <div
                className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
                id="toggleNav1"
              />
            </ul>
          </div>
        </nav>
      </header>
      {/* <div className="sidebar position-absolute">
        <div className="btn-style">
          <Button variant="contained" color="default" className="my-4">
            <Link to="/Home">Home</Link>
          </Button>
          <Button variant="contained" color="default">
            Login
          </Button>
          <div>
            <TwitterIcon size={32} round />
          </div>
          <button type="button" onClick={share}>
            Share
          </button>
        </div>
      </div> */}
      <div
        id="carouselExampleControls"
        className="carousel slide"
        data-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <div className="reward-wrapper">
              <div className="reward-wrapper-left" role="presentation">
                <img src="../../images/user.png" alt="User" />
              </div>
            </div>
            <div className="reward-wrapper-right">
              <div className="home-content">
                <div>
                  <div className="justify-content-between align-items-center">
                    <h1 className="mb-0">Jebson Philip</h1>
                    <div className="mt-4 awardstyle">
                      <img src="../../images/aa.png" alt="Awards" />
                    </div>
                    <div className="content">
                      <h2 className="text-center my-4">Special Award</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam,
                      </p>
                      <div className="d-flex justify-content-between">
                        <div>
                          <p>Appreciate</p>
                          <img src="../../images/clap.png" alt="Appreciate" />
                        </div>
                        <div>
                          <p>Comment</p>
                          <img src="../../images/comment.png" alt="Comment" />
                        </div>
                        <div>
                          <p>Share</p>
                          <img src="../../images/share.png" alt="Share" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="carousel-item">
            <div className="reward-wrapper">
              <div className="reward-wrapper-left" role="presentation">
                <img src="../../images/user.png" alt="User" />
              </div>
            </div>
            <div className="reward-wrapper-right">
              <div className="home-content">
                <div>
                  <div className="justify-content-between align-items-center">
                    <h1 className="mb-0">Rachel Philip</h1>
                    <div className="mt-4 awardstyle">
                      <img src="../../images/aa.png" alt="Awards" />
                    </div>
                    <div className="content">
                      <h2 className="text-center my-4">Special Award</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam,
                      </p>
                      <div className="d-flex justify-content-between">
                        <div>
                          <p>Appreciate</p>
                          <img src="../../images/clap.png" alt="Appreciate" />
                        </div>
                        <div>
                          <p>Comment</p>
                          <img src="../../images/comment.png" alt="Comment" />
                        </div>
                        <div>
                          <p>Share</p>
                          <img src="../../images/share.png" alt="Share" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <a
          className="carousel-control-prev"
          href="#carouselExampleControls"
          role="button"
          data-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleControls"
          role="button"
          data-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>
    </RewardsWrapper>
  );
};

export default RewardsPage;
